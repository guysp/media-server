# media-server
An all-in-one package of services for your media server.

## What's included?
* <img src='http://www.google.com/s2/favicons?domain=plex.tv' height='16' width='16' /> [Plex](https://plex.tv)
* <img src='http://www.google.com/s2/favicons?domain=tautulli.com' height='16' width='16' /> [Tautulli](http://tautulli.com/)
* <img src='http://www.google.com/s2/favicons?domain=radarr.video' height='16' width='16' /> [Radarr](https://radarr.video)
* <img src='http://www.google.com/s2/favicons?domain=sonarr.tv' height='16' width='16' /> [Sonarr](https://sonarr.tv/)
* <img src='https://raw.githubusercontent.com/theotherp/nzbhydra2/master/core/ui-src/img/favicon.ico' height='16' width='16' /> [NZBHydra](https://github.com/theotherp/nzbhydra2)
* <img src='http://www.google.com/s2/favicons?domain=sabnzbd.org' height='16' width='16' /> [SABnzbd](https://sabnzbd.org)
* <img src='https://raw.githubusercontent.com/Jackett/Jackett/9215ae44ba6cc52080bc21142ca56de8319c2e81/src/Jackett.Common/Content/favicon.ico' height='16' width='16' /> [Jackett](https://github.com/Jackett/Jackett)
* <img src='http://www.google.com/s2/favicons?domain=deluge-torrent.org' height='16' width='16' /> [Deluge](https://deluge-torrent.org)
* <img src='http://www.google.com/s2/favicons?domain=heimdall.site' height='16' width='16' /> [Heimdall](https://heimdall.site)
* <img src='http://www.google.com/s2/favicons?domain=openvpn.net' height='16' width='16' /> [OpenVPN](https://openvpn.net/)
* <img src='http://www.google.com/s2/favicons?domain=traefik.io' height='16' width='16' /> [Traefik](https://traefik.io/)
* [ddclient](https://sourceforge.net/p/ddclient/wiki/Home/)

## Getting Started

### Setup
* `npm install`
* `npm docker:build` This will generate the `docker-compose.yml`-file
* Copy the `.env.example` to `.env` and fill in the environment variables.

### Go!
* Run `docker-compose up -d` and you're good to go!

### Configuration
You still need to configure ddclient, OpenVPN and Traefik before you can connect to the services.

#### ddclient (Optional)
Go into the `CONFIG_DIR/ddclient` and change the `ddclient.conf`-file to your preferences.

#### OpenVPN
Go into the `CONFIG_DIR/openvpn` and add your OpenVPN configuration here.
This is highly recommended for using Deluge and Jackett.

#### Traefik
Go into the `CONFIG_DIR/traefik` and create the following files:

- `acme.json`
- `.htpasswd`
- `traefik.toml` - Grab the default configuration here: [traefik.sample.toml](https://github.com/containous/traefik/blob/master/traefik.sample.toml)
Make sure That you enable the `docker`-section!
