var ddclient = require('./ddclient');
var traefik = require('./traefik');
var openvpn = require('./openvpn');
var heimdall = require('./heimdall');
var plex = require('./plex');
var tautulli = require('./tautulli');
var sonarr = require('./sonarr');
var radarr = require('./radarr');
var jackett = require('./jackett');
var nzbhydra = require('./nzbhydra');
var deluge = require('./deluge');
var sabnzbd = require('./sabnzbd');

module.exports = {
    version: '3',
    networks: {
        default: {
            ipam: {
                driver: 'default'
            }
        }
    },
    services: {
        ddclient: ddclient,
        traefik: traefik,
        openvpn: openvpn,
        heimdall: heimdall,
        plex: plex,
        tautulli: tautulli,
        sonarr: sonarr,
        radarr: radarr,
        jackett: jackett,
        nzbhydra: nzbhydra,
        deluge: deluge,
        sabnzbd: sabnzbd
    }
};