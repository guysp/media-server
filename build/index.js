var fs = require('fs');
var YAML = require('json2yaml');
var config = require('../config/index');

var build = function () {
    console.log('Building Docker Compose file...');

    fs.writeFileSync('docker-compose.yml', YAML.stringify(config));

    console.log('Finished building the Docker compose file');
};

build();